package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private Long id;
    private String descricao;
    private String titulo;
    private Boolean mesaRedonda;
    private java.util.Date data;
    private Integer qtdInscricoes;
    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        super();
        this.qtdInscricoes = qtdInscricoes;
        this.areasCientificas.add(areaCientifica);
        professor.adicionarSeminario(this);
        this.professores.add(professor);
        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public java.util.Date getData() {
        return this.data;
    }

    public void setData(java.util.Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

}
